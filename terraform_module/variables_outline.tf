variable "outline_thirdPartyAuth_mattermost_clientId" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_mattermost_clientSecret" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_mattermost_tokenUri" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_mattermost_authorizeUri" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_mattermost_profileUri" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_displayName" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_clientId" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_clientSecret" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_scopes" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_authUri" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_tokenUri" {
  type    = string
  default = null
}
variable "outline_thirdPartyAuth_openidConnect_userInfoUri" {
  type    = string
  default = null
}
variable "outline_secretKey" {
  type    = string
  default = null
  validation {
    condition     = length(regexall("^[0-9a-f]{64}$", var.outline_secretKey)) > 0
    error_message = "Should be 32 bytes of randomness in hex format (64 characters long)"
  }
}
variable "outline_utilsSecret" {
  type    = string
  default = null
  validation {
    condition     = length(regexall("^[0-9a-f]{64}$", var.outline_utilsSecret)) > 0
    error_message = "Should be 32 bytes of randomness in hex format (64 characters long)"
  }
}
variable "outline_url" {
  type    = string
  default = null
}
variable "outline_email_from" {
  type    = string
  default = null
}
variable "outline_email_reply" {
  type    = string
  default = null
}
variable "outline_email_smtp_host" {
  type    = string
  default = null
}
variable "outline_email_smtp_user" {
  type    = string
  default = null
}
variable "outline_email_smtp_password" {
  type    = string
  default = null
}
variable "outline_email_smtp_port" {
  type    = string
  default = null
}
variable "outline_storage_s3_bucket" {
  type    = string
  default = null
}
variable "outline_storage_s3_endpoint" {
  type    = string
  default = null
}
variable "outline_storage_s3_key" {
  type    = string
  default = null
}
variable "outline_storage_s3_region" {
  type    = string
  default = null
}
variable "outline_storage_s3_secret" {
  type    = string
  default = null
}
variable "outline_storage_s3_uploadMaxSize" {
  type    = string
  default = null
}
variable "outline_storage_s3_acl" {
  type    = string
  default = "public-read"
}
variable "outline_postgresql_deploy" {
  type    = string
  default = null
}
variable "outline_postgresql_postgresqlUsername" {
  type    = string
  default = null
}
variable "outline_postgresql_postgresqlPassword" {
  type    = string
  default = null
}
variable "outline_postgresql_postgresqlDatabase" {
  type    = string
  default = null
}
variable "outline_postgresql_postgresqlHost" {
  type    = string
  default = null
}
variable "outline_postgresql_postgresqlUrl" {
  type    = string
  default = null
}
variable "outline_postgresql_port" {
  type    = string
  default = null
}
variable "outline_redis_deploy" {
  type    = string
  default = null
}
variable "outline_redis_redisUsername" {
  type    = string
  default = null
}
variable "outline_redis_redisPassword" {
  type    = string
  default = null
}
variable "outline_redis_redisHost" {
  type    = string
  default = null
}
variable "outline_redis_redisUrl" {
  type    = string
  default = null
}
variable "outline_redis_port" {
  type    = string
  default = null
}
