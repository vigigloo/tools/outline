resource "helm_release" "outline" {
  chart           = "outline"
  repository      = "https://gitlab.com/api/v4/projects/30008190/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  set_sensitive {
    name  = "secretKey"
    value = var.outline_secretKey
    type  = "string"
  }
  set_sensitive {
    name  = "utilsSecret"
    value = var.outline_utilsSecret
    type  = "string"
  }
  set_sensitive {
    name  = "url"
    value = var.outline_url
    type  = "string"
  }

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress_host == null ? [] : [var.ingress_host]
    content {
      name  = "ingress.host"
      value = var.ingress_host
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_thirdPartyAuth_mattermost_clientId == null ? [] : [var.outline_thirdPartyAuth_mattermost_clientId]
    content {
      name  = "thirdPartyAuth.mattermost.clientId"
      value = var.outline_thirdPartyAuth_mattermost_clientId
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_thirdPartyAuth_mattermost_clientSecret == null ? [] : [var.outline_thirdPartyAuth_mattermost_clientSecret]
    content {
      name  = "thirdPartyAuth.mattermost.clientSecret"
      value = var.outline_thirdPartyAuth_mattermost_clientSecret
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_mattermost_tokenUri == null ? [] : [var.outline_thirdPartyAuth_mattermost_tokenUri]
    content {
      name  = "thirdPartyAuth.mattermost.tokenUri"
      value = var.outline_thirdPartyAuth_mattermost_tokenUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_mattermost_authorizeUri == null ? [] : [var.outline_thirdPartyAuth_mattermost_authorizeUri]
    content {
      name  = "thirdPartyAuth.mattermost.authorizeUri"
      value = var.outline_thirdPartyAuth_mattermost_authorizeUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_mattermost_profileUri == null ? [] : [var.outline_thirdPartyAuth_mattermost_profileUri]
    content {
      name  = "thirdPartyAuth.mattermost.profileUri"
      value = var.outline_thirdPartyAuth_mattermost_profileUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_openidConnect_displayName == null ? [] : [var.outline_thirdPartyAuth_openidConnect_displayName]
    content {
      name  = "thirdPartyAuth.openidConnect.displayName"
      value = var.outline_thirdPartyAuth_openidConnect_displayName
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_thirdPartyAuth_openidConnect_clientId == null ? [] : [var.outline_thirdPartyAuth_openidConnect_clientId]
    content {
      name  = "thirdPartyAuth.openidConnect.clientId"
      value = var.outline_thirdPartyAuth_openidConnect_clientId
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_thirdPartyAuth_openidConnect_clientSecret == null ? [] : [var.outline_thirdPartyAuth_openidConnect_clientSecret]
    content {
      name  = "thirdPartyAuth.openidConnect.clientSecret"
      value = var.outline_thirdPartyAuth_openidConnect_clientSecret
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_openidConnect_scopes == null ? [] : [var.outline_thirdPartyAuth_openidConnect_scopes]
    content {
      name  = "thirdPartyAuth.openidConnect.scopes"
      value = var.outline_thirdPartyAuth_openidConnect_scopes
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_openidConnect_authUri == null ? [] : [var.outline_thirdPartyAuth_openidConnect_authUri]
    content {
      name  = "thirdPartyAuth.openidConnect.authUri"
      value = var.outline_thirdPartyAuth_openidConnect_authUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_openidConnect_tokenUri == null ? [] : [var.outline_thirdPartyAuth_openidConnect_tokenUri]
    content {
      name  = "thirdPartyAuth.openidConnect.tokenUri"
      value = var.outline_thirdPartyAuth_openidConnect_tokenUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_thirdPartyAuth_openidConnect_userInfoUri == null ? [] : [var.outline_thirdPartyAuth_openidConnect_userInfoUri]
    content {
      name  = "thirdPartyAuth.openidConnect.userInfoUri"
      value = var.outline_thirdPartyAuth_openidConnect_userInfoUri
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_email_from == null ? [] : [var.outline_email_from]
    content {
      name  = "email.from"
      value = var.outline_email_from
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_email_reply == null ? [] : [var.outline_email_reply]
    content {
      name  = "email.reply"
      value = var.outline_email_reply
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_email_smtp_host == null ? [] : [var.outline_email_smtp_host]
    content {
      name  = "email.smtp.host"
      value = var.outline_email_smtp_host
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_email_smtp_user == null ? [] : [var.outline_email_smtp_user]
    content {
      name  = "email.smtp.user"
      value = var.outline_email_smtp_user
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_email_smtp_password == null ? [] : [var.outline_email_smtp_password]
    content {
      name  = "email.smtp.password"
      value = var.outline_email_smtp_password
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_email_smtp_port == null ? [] : [var.outline_email_smtp_port]
    content {
      name  = "email.smtp.port"
      value = var.outline_email_smtp_port
    }
  }
  dynamic "set" {
    for_each = var.outline_storage_s3_bucket == null ? [] : [var.outline_storage_s3_bucket]
    content {
      name  = "storage.s3.bucket"
      value = var.outline_storage_s3_bucket
    }
  }
  dynamic "set" {
    for_each = var.outline_storage_s3_endpoint == null ? [] : [var.outline_storage_s3_endpoint]
    content {
      name  = "storage.s3.endpoint"
      value = var.outline_storage_s3_endpoint
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_storage_s3_key == null ? [] : [var.outline_storage_s3_key]
    content {
      name  = "storage.s3.key"
      value = var.outline_storage_s3_key
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_storage_s3_region == null ? [] : [var.outline_storage_s3_region]
    content {
      name  = "storage.s3.region"
      value = var.outline_storage_s3_region
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_storage_s3_secret == null ? [] : [var.outline_storage_s3_secret]
    content {
      name  = "storage.s3.secret"
      value = var.outline_storage_s3_secret
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_storage_s3_uploadMaxSize == null ? [] : [var.outline_storage_s3_uploadMaxSize]
    content {
      name  = "storage.s3.uploadMaxSize"
      value = var.outline_storage_s3_uploadMaxSize
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_storage_s3_acl == null ? [] : [var.outline_storage_s3_acl]
    content {
      name  = "storage.s3.acl"
      value = var.outline_storage_s3_acl
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_postgresql_deploy == null ? [] : [var.outline_postgresql_deploy]
    content {
      name  = "postgresql.deploy"
      value = var.outline_postgresql_deploy
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_postgresql_postgresqlUsername == null ? [] : [var.outline_postgresql_postgresqlUsername]
    content {
      name  = "postgresql.postgresqlUsername"
      value = var.outline_postgresql_postgresqlUsername
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_postgresql_postgresqlPassword == null ? [] : [var.outline_postgresql_postgresqlPassword]
    content {
      name  = "postgresql.postgresqlPassword"
      value = var.outline_postgresql_postgresqlPassword
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_postgresql_postgresqlDatabase == null ? [] : [var.outline_postgresql_postgresqlDatabase]
    content {
      name  = "postgresql.postgresqlDatabase"
      value = var.outline_postgresql_postgresqlDatabase
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_postgresql_postgresqlHost == null ? [] : [var.outline_postgresql_postgresqlHost]
    content {
      name  = "postgresql.postgresqlHost"
      value = var.outline_postgresql_postgresqlHost
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_postgresql_postgresqlUrl == null ? [] : [var.outline_postgresql_postgresqlUrl]
    content {
      name  = "postgresql.postgresqlUrl"
      value = var.outline_postgresql_postgresqlUrl
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_postgresql_port == null ? [] : [var.outline_postgresql_port]
    content {
      name  = "postgresql.service.port"
      value = var.outline_postgresql_port
    }
  }
  dynamic "set" {
    for_each = var.outline_redis_deploy == null ? [] : [var.outline_redis_deploy]
    content {
      name  = "redis.deploy"
      value = var.outline_redis_deploy
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_redis_redisUsername == null ? [] : [var.outline_redis_redisUsername]
    content {
      name  = "redis.redisUsername"
      value = var.outline_redis_redisUsername
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_redis_redisPassword == null ? [] : [var.outline_redis_redisPassword]
    content {
      name  = "redis.redisPassword"
      value = var.outline_redis_redisPassword
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_redis_redisHost == null ? [] : [var.outline_redis_redisHost]
    content {
      name  = "redis.redisHost"
      value = var.outline_redis_redisHost
      type  = "string"
    }
  }
  dynamic "set_sensitive" {
    for_each = var.outline_redis_redisUrl == null ? [] : [var.outline_redis_redisUrl]
    content {
      name  = "redis.redisUrl"
      value = var.outline_redis_redisUrl
      type  = "string"
    }
  }
  dynamic "set" {
    for_each = var.outline_redis_port == null ? [] : [var.outline_redis_port]
    content {
      name  = "redis.service.port"
      value = var.outline_redis_port
    }
  }
}
