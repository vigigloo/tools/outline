{{- define "outline.env" -}}
- name: SECRET_KEY
  value: {{ .Values.secretKey | default (randAscii 64) | quote }}
- name: UTILS_SECRET
  value: {{ .Values.utilsSecret | default (randAscii 64) | quote }}
- name: URL
  value: {{ .Values.url | quote }}
{{- with .Values.postgresql }}
{{- if .postgresqlUrl }}
- name: DATABASE_URL
  value: {{ .postgresqlUrl | squote }}
{{- else if .postgresqlHost }}
- name: DATABASE_URL
  value: postgresql://{{ .postgresqlUsername }}:{{ .postgresqlPassword }}@{{ if .deploy }}{{ printf "%s-%s" $.Release.Name "postgresql" | trunc 63 | trimSuffix "-" }}{{ else }}{{ .postgresqlHost }}{{ end }}:{{ .service.port | default "5432" }}/{{ .postgresqlDatabase }}
{{- end }}
{{- end }}{{/* end with */}}
{{- with .Values.redis }}
{{- if .redisUrl }}
- name: REDIS_URL
  value: {{ .redisUrl | squote }}
{{- else if .redisHost }}
- name: REDIS_URL
  value: redis://{{ .redisUsername }}:{{ .redisPassword }}@{{ if .deploy }}{{ printf "%s-%s" $.Release.Name "redis" | trunc 63 | trimSuffix "-" }}{{ else }}{{ .redisHost }}{{ end }}:{{ .service.port | default "6379" }}/{{ .redisDatabase }}
{{- end }}
{{- end }}{{/* end with */}}
{{- with .Values.email }}
{{- if . }}
{{- if .from }}
- name: SMTP_FROM_EMAIL
  value: {{ .from | quote }}
{{- end }}
{{- if .reply }}
- name: SMTP_REPLY_EMAIL
  value: {{ .reply | quote }}
{{- end }}
{{- with .smtp }}
{{- if . }}
- name: SMTP_HOST
  value: {{ .host | quote }}
- name: SMTP_PASSWORD
  value: {{ .password | quote }}
- name: SMTP_PORT
  value: {{ .port | quote }}
- name: SMTP_USERNAME
  value: {{ .user | quote }}
{{- end }}{{/*end if smtp*/}}
{{- end}}{{/* end with smtp */}}
{{- end }}{{/*end if email*/}}
{{- end }}{{/* end with email */}}
{{- with .Values.storage }}
{{- with .s3 }}
{{- if . }}
- name: AWS_S3_UPLOAD_BUCKET_NAME
  value: {{ .bucket | quote }}
- name: AWS_S3_UPLOAD_BUCKET_URL
  value: {{ .endpoint | quote }}
- name: AWS_ACCESS_KEY_ID
  value: {{ .key | quote }}
- name: AWS_REGION
  value: {{ .region | quote }}
- name: AWS_SECRET_ACCESS_KEY
  value: {{ .secret | quote }}
- name: AWS_S3_UPLOAD_MAX_SIZE
  value: {{ .uploadMaxSize | quote }}
- name: AWS_S3_ACL
  value: {{ .acl | quote }}
{{- end }}{{/*end if s3*/}}
{{- end}}{{/* end with s3 */}}
{{- end }}{{/* end with storage */}}
{{- with .Values.thirdPartyAuth.mattermost }}
- name: MATTERMOST_CLIENT_ID
  value: {{ .clientId | quote }}
- name: MATTERMOST_CLIENT_SECRET
  value: {{ .clientSecret | quote }}
- name: MATTERMOST_TOKEN_URL
  value: {{ .tokenUri | quote }}
- name: MATTERMOST_AUTHORIZE_URL
  value: {{ .authorizeUri | quote }}
- name: MATTERMOST_PROFILE_URL
  value: {{ .profileUri | quote }}
{{- end }}
{{- with .Values.thirdPartyAuth.openidConnect }}
- name: OIDC_DISPLAY_NAME
  value: {{ .displayName | quote }}
- name: OIDC_CLIENT_ID
  value: {{ .clientId | quote }}
- name: OIDC_CLIENT_SECRET
  value: {{ .clientSecret | quote }}
- name: OIDC_SCOPES
  value: {{ .scopes | quote }}
- name: OIDC_AUTH_URI
  value: {{ .authUri | quote }}
- name: OIDC_TOKEN_URI
  value: {{ .tokenUri | quote }}
- name: OIDC_USERINFO_URI
  value: {{ .userInfoUri | quote }}
{{- end }}
{{- end }}{{/* end define */}}
